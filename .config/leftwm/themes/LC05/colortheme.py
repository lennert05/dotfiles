import pywal
import sys,os
import json
dirname = os.path.dirname(__file__)
backgroundslocation = os.path.join(dirname, "./backgrounds")
cachelocation = os.path.join(dirname, "./colors")
colorsrofi = os.path.join(dirname, "./rofi/colors.rasi")
colorjsonpath = os.path.join(dirname, "./colors/colors.json")
colorsjson = open(colorjsonpath, "r")
colors = json.load(colorsjson)

def main():
    image = pywal.image.get(backgroundslocation)
    colors = pywal.colors.get(image, cachelocation, 16)
    pywal.export.every(colors, cachelocation)
    pywal.wallpaper.change(image)

    with open(colorsrofi, "w") as outfile:
        outfile.write('*{\n')
        outfile.write('al:'+colors['special']['foreground']+';\n')
        outfile.write('bg:'+colors['special']['foreground']+';\n')
        outfile.write('se:'+colors['colors']['color4']+';\n')
        outfile.write('fg:'+colors['special']['background']+';\n')
        outfile.write('ac:'+colors['colors']['color1']+';\n')
        outfile.write('}')
        outfile.close()

main()
