static const char norm_fg[] = "#02051D";
static const char norm_bg[] = "#f6f1f7";
static const char norm_border[] = "#78627a";

static const char sel_fg[] = "#02051D";
static const char sel_bg[] = "#2F59BC";
static const char sel_border[] = "#02051D";

static const char urg_fg[] = "#02051D";
static const char urg_bg[] = "#234AA0";
static const char urg_border[] = "#234AA0";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
