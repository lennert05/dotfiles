" Special
let wallpaper  = "/home/lennert/.config/leftwm/themes/LC05/backgrounds/wallhaven-wqqlox_1920x1080.png"
let background = "#f6f1f7"
let foreground = "#02051D"
let cursor     = "#02051D"

" Colors
let color0  = "#f6f1f7"
let color1  = "#234AA0"
let color2  = "#2F59BC"
let color3  = "#5857A6"
let color4  = "#6470C9"
let color5  = "#9171B5"
let color6  = "#7485DA"
let color7  = "#02051D"
let color8  = "#78627a"
let color9  = "#234AA0"
let color10 = "#2F59BC"
let color11 = "#5857A6"
let color12 = "#6470C9"
let color13 = "#9171B5"
let color14 = "#7485DA"
let color15 = "#02051D"
