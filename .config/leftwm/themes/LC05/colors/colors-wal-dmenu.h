static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#02051D", "#f6f1f7" },
	[SchemeSel] = { "#02051D", "#234AA0" },
	[SchemeOut] = { "#02051D", "#7485DA" },
};
