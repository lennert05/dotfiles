const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#f6f1f7", /* black   */
  [1] = "#234AA0", /* red     */
  [2] = "#2F59BC", /* green   */
  [3] = "#5857A6", /* yellow  */
  [4] = "#6470C9", /* blue    */
  [5] = "#9171B5", /* magenta */
  [6] = "#7485DA", /* cyan    */
  [7] = "#02051D", /* white   */

  /* 8 bright colors */
  [8]  = "#78627a",  /* black   */
  [9]  = "#234AA0",  /* red     */
  [10] = "#2F59BC", /* green   */
  [11] = "#5857A6", /* yellow  */
  [12] = "#6470C9", /* blue    */
  [13] = "#9171B5", /* magenta */
  [14] = "#7485DA", /* cyan    */
  [15] = "#02051D", /* white   */

  /* special colors */
  [256] = "#f6f1f7", /* background */
  [257] = "#02051D", /* foreground */
  [258] = "#02051D",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
