--Place this file in your .xmonad/lib directory and import module Colors into .xmonad/xmonad.hs config
--The easy way is to create a soft link from this file to the file in .xmonad/lib using ln -s
--Then recompile and restart xmonad.

module Colors
    ( wallpaper
    , background, foreground, cursor
    , color0, color1, color2, color3, color4, color5, color6, color7
    , color8, color9, color10, color11, color12, color13, color14, color15
    ) where

-- Shell variables
-- Generated by 'wal'
wallpaper="/home/lennert/.config/leftwm/themes/LC05/backgrounds/wallhaven-wqqlox_1920x1080.png"

-- Special
background="#f6f1f7"
foreground="#02051D"
cursor="#02051D"

-- Colors
color0="#f6f1f7"
color1="#234AA0"
color2="#2F59BC"
color3="#5857A6"
color4="#6470C9"
color5="#9171B5"
color6="#7485DA"
color7="#02051D"
color8="#78627a"
color9="#234AA0"
color10="#2F59BC"
color11="#5857A6"
color12="#6470C9"
color13="#9171B5"
color14="#7485DA"
color15="#02051D"
